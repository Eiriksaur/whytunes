# README

A Spring project offering an API where the user can query a database returning customers and the songs they have bought
with the logic around it (artists, invoice, genre, etc).

The project also has a website the uses Thymeleaf where the user can search for songs in the database.

## API calls
There is a folder at the top-level of the project called postmanCollection containing samples of all the possible API calls.

## HEROKU 
https://noroff-task5-thymeleaf.herokuapp.com/