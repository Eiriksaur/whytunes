package com.noroff.WhyTunes.models;

public class InvoiceCustomer {
    private String firstName;
    private String lastName;
    private int total;

    public InvoiceCustomer(){

    }

    public InvoiceCustomer(String firstName, String lastName, int total){
        this.firstName = firstName;
        this.lastName = lastName;
        this.total = total;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
