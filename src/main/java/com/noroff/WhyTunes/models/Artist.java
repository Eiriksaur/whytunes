package com.noroff.WhyTunes.models;

public class Artist {

    private String fullName;

    public Artist(String fullName){
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
