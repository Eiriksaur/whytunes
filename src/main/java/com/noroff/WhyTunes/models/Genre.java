package com.noroff.WhyTunes.models;

public class Genre {
    private String name;
    private int numberOfTracks;

    public Genre(String name){
        this.name = name;
    }

    public Genre(String name, int numberOfTracks){
        this.name = name;
        this.numberOfTracks = numberOfTracks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfTracks() {
        return numberOfTracks;
    }

    public void setNumberOfTracks(int numberOfTracks) {
        this.numberOfTracks = numberOfTracks;
    }
}
