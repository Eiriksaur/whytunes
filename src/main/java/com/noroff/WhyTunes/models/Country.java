package com.noroff.WhyTunes.models;

public class Country {
    private String countryName;
    private int numberOfCostumers;

    public Country(){

    }

    public Country(String countryName, int numberOfCostumers){
        this.countryName = countryName;
        this.numberOfCostumers = numberOfCostumers;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getNumberOfCostumers() {
        return numberOfCostumers;
    }

    public void setNumberOfCostumers(int numberOfCostumers) {
        this.numberOfCostumers = numberOfCostumers;
    }
}
