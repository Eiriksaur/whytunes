package com.noroff.WhyTunes.data_access;

import com.noroff.WhyTunes.models.Country;
import com.noroff.WhyTunes.models.Customer;
import com.noroff.WhyTunes.models.Genre;
import com.noroff.WhyTunes.models.InvoiceCustomer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {

    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    //Getting all the customers from the db
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers= new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, " +
                    "Country,PostalCode, Phone FROM Customer");
            ResultSet results = prep.executeQuery();
            //Looping through the results and making new customer objects to add to the list
            while(results.next()){
                customers.add(new Customer(
                        results.getInt("CustomerId"),
                        results.getString("FirstName"),
                        results.getString("LastName"),
                        results.getString("Country"),
                        results.getString("PostalCode"),
                        results.getString("Phone")
                ));
            }
            System.out.println("Success");
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try{
                //Closing the connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    //Creating a single new customer in the db
    public boolean createCustomer(Customer customer){
        boolean success = false;
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            //Getting a random supportRep
            int repId = 1;
            PreparedStatement prep = conn.prepareStatement("SELECT EmployeeId FROM Employee " +
                    "ORDER BY RANDOM() LIMIT 1");
            ResultSet results = prep.executeQuery();
            while(results.next()){
                repId = results.getInt("EmployeeId");
            }
            //Creating the new customer with the random repID
            prep = conn.prepareStatement("INSERT INTO Customer(CustomerId, FirstName, " +
                    "LastName, Country,PostalCode, Phone, SupportRepId, Email) VALUES(?,?,?,?,?,?,?,?)");
            prep.setInt(1, customer.getCustomerId());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCountry());
            prep.setString(5, customer.getPostalCode());
            prep.setString(6, customer.getPhonenumber());
            prep.setString(7, "email@mail.com"); //This field could not be null
            prep.setInt(8, repId);

            int res = prep.executeUpdate();
            success = res != 0 ? true : false; //Determining what the return value is
            System.out.println("Success");
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try{
                //Closing connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return success;
    }

    //Updating an existing customer in the db
    public boolean updateCustomer(Customer customer){
        boolean success = false;
        try{
            //Connectiong to db
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("UPDATE Customer SET CustomerId=? , FirstName=?, " +
                    "LastName=?, Country=?,PostalCode=?, Phone=? WHERE CustomerId=?");
            prep.setInt(1, customer.getCustomerId());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCountry());
            prep.setString(5, customer.getPostalCode());
            prep.setString(6, customer.getPhonenumber());
            prep.setInt(7, customer.getCustomerId());

            int res = prep.executeUpdate();
            //Determening return value
            success = res != 0 ? true : false;

            System.out.println("Success");
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try{
                //Closing connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return success;
    }

    /*Getting all the countries registered with customers in the db
    sorted by number of customers */
    public ArrayList<Country> getCountriesByCustomer(){
        ArrayList<Country> countryList = new ArrayList<>();
        try{
            //Connecting to the db
            conn = DriverManager.getConnection(URL);
            //Selecting every country found in Employee-table, counting them
            // and grouping matching countries together in descending order
            PreparedStatement prep = conn.prepareStatement("SELECT Country, COUNT(*) FROM Customer" +
                    " GROUP BY Country ORDER BY COUNT(*) DESC");
            ResultSet results = prep.executeQuery();
            //Looping through and making "country-objects"
            while(results.next()){
                countryList.add(new Country(
                        results.getString("Country"),
                        results.getInt("COUNT(*)")
                ));
            }
            System.out.println("Success");
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try{
                //Closing connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return countryList;
    }

    /*Getting all the customers from the db
    sorted by their invoice */
    public ArrayList<InvoiceCustomer> getCustomerByInvoice(){
        ArrayList<InvoiceCustomer> customers = new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            //Using an inner-join on the employee and invoice table
            //Making it possible to tie a customer to the total value in the invoice table
            PreparedStatement prep = conn.prepareStatement("SELECT Firstname, Lastname, Total FROM Customer" +
                    " INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId ORDER BY Total DESC");
            ResultSet results = prep.executeQuery();
            //Looping through and making "invoice-customer" objects
            while(results.next()){
                customers.add(new InvoiceCustomer(
                        results.getString("Firstname"),
                        results.getString("Lastname"),
                        results.getInt("Total")
                ));
            }
            System.out.println("Success");
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try{
                //Closing connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    /*Getting all the favourite genre for a single customer
    in the case of ties both genres are listed */
    public ArrayList<Genre> getFavouriteGenre(int customerId){
        ArrayList<Genre> genre = new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            /*
            Making a nested SQL to make sure we are able to extract multiple MAX results
            Inner-joins from the Genre-table all the way to the Customer-table
            Counting the number of tracks with the same genre name and checking
            if it is a MAX value.
             */
            PreparedStatement prep = conn.prepareStatement("SELECT genreName, genreCount FROM(" +
                    " SELECT Genre.Name as genreName, COUNT(*) as genreCount" +
                    " FROM Track" +
                    " INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                    " INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId" +
                    " INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId" +
                    " INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId" +
                    " WHERE Customer.CustomerId=?" +
                    " GROUP BY Genre.Name )" +
                    " WHERE genreCount = ( SELECT MAX(genreCount)" +
                                           " FROM ( SELECT Genre.Name as genreName, COUNT(*) as genreCount" +
                                            " FROM Track" +
                                            " INNER JOIN Genre ON Genre.GenreId = Track.GenreId"+
                                            " INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId"+
                                            " INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId"+
                                            " INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId"+
                                            " WHERE Customer.CustomerId=?" +
                                            " GROUP BY Genre.Name" +
                                                ")" +
                                          ")"
            );
            prep.setInt(1, customerId);
            prep.setInt(2, customerId);
            ResultSet results = prep.executeQuery();
            //Looping through and making "genre" objects
            while(results.next()){
                genre.add(new Genre(
                        results.getString("genreName"),
                        results.getInt("genreCount")
                ));
            }
            System.out.println("Success");
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try{
                //Closing connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return genre;
    }

}
