package com.noroff.WhyTunes.data_access;

import com.noroff.WhyTunes.models.Artist;
import com.noroff.WhyTunes.models.Genre;
import com.noroff.WhyTunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {

    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<Track> getFiveRandomTracks(){
        ArrayList<Track> tracksList = new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            //Getting 5 random tracks
            PreparedStatement prep = conn.prepareStatement("SELECT Track.Name as name FROM Track " +
                    "ORDER BY RANDOM() LIMIT 5");
            ResultSet results = prep.executeQuery();
            //Looping through and creating tracks in the tracklist
            while(results.next()){
                tracksList.add(new Track(results.getString("name")));
            }
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try {
                //Closing the connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return tracksList;
    }

    public ArrayList<Artist> getFiveRandomArtists(){
        ArrayList<Artist> artistList = new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            //Getting 5 random tracks
            PreparedStatement prep = conn.prepareStatement("SELECT Artist.Name as name FROM Artist " +
                    "ORDER BY RANDOM() LIMIT 5");
            ResultSet results = prep.executeQuery();
            //Looping through and creating artists in the artistlist
            while(results.next()){
                artistList.add(new Artist(results.getString("name")));
            }
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try {
                //Closing the connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return artistList;
    }

    public ArrayList<Genre> getFiveRandomGenres(){
        ArrayList<Genre> genreList = new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            //Getting 5 random tracks
            PreparedStatement prep = conn.prepareStatement("SELECT Genre.Name as name FROM Genre " +
                    "ORDER BY RANDOM() LIMIT 5");
            ResultSet results = prep.executeQuery();
            //Looping through and creating genres in the genreList
            while(results.next()){
                genreList.add(new Genre(results.getString("name")));
            }
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try {
                //Closing the connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return genreList;
    }

    public ArrayList<Track> getMatchingSongs(String searchTerm){
        ArrayList<Track> tracklist = new ArrayList<>();
        try{
            //Connecting to db
            conn = DriverManager.getConnection(URL);
            //Getting all songs that match the searchterm
            PreparedStatement prep = conn.prepareStatement("SELECT Track.Name as trackname," +
                    " Artist.Name as artistname, Album.Title as albumname, Genre.Name as genrename FROM Track" +
                    " INNER JOIN Album ON Track.AlbumId = Album.AlbumId" +
                    " INNER JOIN Artist ON Album.ArtistID = Artist.ArtistId" +
                    " INNER JOIN Genre ON Track.GenreId = Genre.GenreId" +
                    " WHERE trackname LIKE ?");
            prep.setString(1, searchTerm.trim());
            ResultSet results = prep.executeQuery();
            //Looping through and creating tracks in the trackslist
            while(results.next()){
                tracklist.add(new Track(
                        results.getString("trackname"),
                        results.getString("artistname"),
                        results.getString("albumname"),
                        results.getString("genrename")
                ));
            }
        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            try {
                //Closing the connection
                conn.close();
            }catch (Exception ex){
                System.out.println(ex.toString());
            }
        }
        return tracklist;
    }
}
