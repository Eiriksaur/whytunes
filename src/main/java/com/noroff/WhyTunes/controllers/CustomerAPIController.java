package com.noroff.WhyTunes.controllers;

import com.noroff.WhyTunes.data_access.CustomerRepository;
import com.noroff.WhyTunes.models.Country;
import com.noroff.WhyTunes.models.Customer;
import com.noroff.WhyTunes.models.Genre;
import com.noroff.WhyTunes.models.InvoiceCustomer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;

@RestController
public class CustomerAPIController {
    //Controller for the api

    CustomerRepository repo = new CustomerRepository();

    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ResponseEntity<?> getAllCustomers(){
        ArrayList<Customer> customers= repo.getAllCustomers();
        if(!customers.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(customers);
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public ResponseEntity<?> createCustomer(@RequestBody Customer customer){
        if(customer.getFirstName() == null || customer.getLastName() == null ||
                customer.getCustomerId() == 0 || customer.getCountry() == null
                || customer.getPhonenumber() == null || customer.getPostalCode() == null){
            return new ResponseEntity<>("Incomplete customer object", HttpStatus.BAD_REQUEST);
        }
        boolean created = repo.createCustomer(customer);
        if(!created){
            return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(customer);
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.PUT)
    public ResponseEntity<?> updateCustomer(@RequestBody Customer customer){
        if(customer.getFirstName() == null || customer.getLastName() == null ||
                customer.getCustomerId() == 0 || customer.getCountry() == null
                || customer.getPhonenumber() == null || customer.getPostalCode() == null){
            return new ResponseEntity<>("Incomplete customer object", HttpStatus.BAD_REQUEST);
        }
        boolean updated = repo.updateCustomer(customer);
        if(!updated){
            return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.status(HttpStatus.OK).body(customer);
    }

    @RequestMapping(value = "/api/customers/country", method = RequestMethod.GET)
    public ResponseEntity<?> getCountriesByCustomer(){
        ArrayList<Country> countries= repo.getCountriesByCustomer();
        if(!countries.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(countries);
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
    }

    @RequestMapping(value = "/api/customers/invoice-total", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomerByInvoice(){
        ArrayList<InvoiceCustomer> invoiceCustomers= repo.getCustomerByInvoice();
        if(!invoiceCustomers.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(invoiceCustomers);
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
    }

    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public ResponseEntity<?> getFavouriteGenre(@PathVariable int id){
        ArrayList<Genre> genres= repo.getFavouriteGenre(id);
        if(!genres.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(genres);
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
    }
}
