package com.noroff.WhyTunes.controllers;

import com.noroff.WhyTunes.data_access.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TrackController {

    TrackRepository trackRepo = new TrackRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("tracks", trackRepo.getFiveRandomTracks());
        model.addAttribute("artists", trackRepo.getFiveRandomArtists());
        model.addAttribute("genres", trackRepo.getFiveRandomGenres());
        return "index";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(Model model, @RequestParam String name){
        model.addAttribute("tracks", trackRepo.getMatchingSongs("%"+name+"%"));
        return "search";
    }
}
